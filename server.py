# coding: utf-8
import random
import threading
import time

import zmq
from zmq.eventloop import ioloop, zmqstream
ioloop.install()

import tornado
import tornado.log
import tornado.options
import tornado.web
import tornado.websocket

ZMQ_BIND_ADDR = 'tcp://127.0.0.1:5555'


def slow_responder():
    '''thread for slowly responding to replies.'''
    ctx = zmq.Context.instance()
    socket = ctx.socket(zmq.REP)
    socket.linger = 0
    socket.bind(ZMQ_BIND_ADDR)
    i = 0
    while True:
        message = socket.recv_string()
        tornado.log.app_log.info('worker received {}'.format(message))
        time.sleep(random.randint(1, 3))
        socket.send_string('{} to you too, #{}'.format(message, i))
        i += 1


class EchoHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('echo.html')


class EchoWebSocketHandler(tornado.websocket.WebSocketHandler):
    def open(self):
        tornado.log.app_log.debug('WebSocket opened')

    def on_message(self, message):
        ctx = zmq.Context.instance()
        socket = ctx.socket(zmq.REQ)
        socket.connect(ZMQ_BIND_ADDR)
        # send request to worker
        socket.send_string(message)
        self.stream = zmqstream.ZMQStream(socket)
        self.stream.on_recv(self.handle_reply)

    def handle_reply(self, message):
        # finish web request with worker's reply
        reply = message[0]
        tornado.log.app_log.info('finishing with {}'.format(reply))
        self.stream.close()
        self.write_message('You said: {}'.format(reply))

    def on_close(self):
        tornado.log.app_log.debug('WebSocket closed')


def make_app():
    settings = {
        'debug': True,
        'template_path': './templates',
        'static_path': './static',
    }
    return tornado.web.Application([
        (r'/echo', EchoHandler),
        (r'/websocket', EchoWebSocketHandler),
    ], **settings)


def main():
    tornado.options.parse_command_line()
    tornado.log.app_log.info('Server started...')
    worker = threading.Thread(target=slow_responder)
    worker.daemon = True
    worker.start()

    app = make_app()
    app.listen(8888)
    try:
        tornado.ioloop.IOLoop.current().start()
    except KeyboardInterrupt:
        tornado.log.app_log.info('Server stopped.')


if __name__ == '__main__':
    main()
