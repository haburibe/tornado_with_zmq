(function(){
	var echo = {};
	echo.Message = function(data) {
		this.message = m.prop(data.message);
	};
	echo.MessageList = Array;

	echo.vm = {
		init: function() {
			// WebSocket
			var ws = new WebSocket('ws://localhost:8888/websocket');
			ws.onmessage = function(event) {
				echo.vm.list.unshift(new echo.Message({message: event.data}));
				echo.vm.message('');
				m.redraw();
			};

			echo.vm.list = new echo.MessageList();
			echo.vm.message = m.prop('');
			echo.vm.add = function(event) {
				if (echo.vm.message()) {
					ws.send(echo.vm.message());
				}
			};
		},
	};

	echo.controller = function() {
		echo.vm.init();
	};

	echo.view = function() {
		return [
			m('input', {
				onchange: m.withAttr('value', echo.vm.message),
				value: echo.vm.message(),
			}),
			m('button', {onclick: echo.vm.add}, 'send'),
			m('table', [
				echo.vm.list.map(function(message, index) {
					return m('tr', [
						m('td', message.message())
					]);
				})
			]),
		];
	};

	m.mount(
		document.getElementById('root'),
		{
			controller: echo.controller, 
			view: echo.view,
		}
	);
}());
